﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using System.Windows.Forms;
using AppSAPOpera.Properties;

namespace AppSAPOpera
{
    class ConexionSBO
    {
        #region Globales
        Encoding Encoding = Encoding.ASCII;
        public string sPathExcel = ConectarExcelPATH();
        public static string strRuta = (Application.StartupPath + "\\").Replace(System.IO.Path.DirectorySeparatorChar, System.IO.Path.AltDirectorySeparatorChar);
        #endregion Globales

        public static string ConectarExcelPATH()
        {
            return Properties.Settings.Default.PATH;
        }

        #region conexion
        public Company sboCompany = new Company();

        Company oCompany
        {
            set
            {
                this.sboCompany = value;
            }
            get
            {
                return sboCompany;
            }
        }
        public void ConectarOLD()
        {

            try
            {
                SAPbobsCOM.BoDataServerTypes DbServerType = (SAPbobsCOM.BoDataServerTypes)0;
                string DbServerTypeXML = null;
                string Server = null, DbUserName = null, DbPassword = null, CompanyDB = null;
                string UserName = null, Password = null;

                Server = Settings.Default.SERVER;// "DESKTOP-VQNGFGH";
                DbServerTypeXML = Settings.Default.TYPOSERVER; //"MS 2014";
                DbUserName = Settings.Default.BDUSER; //"sa";
                DbPassword = Settings.Default.BDKEY;//"B1Admin";
                CompanyDB = Settings.Default.DATABASE;//"SBO_DELMONTE";
                UserName = Settings.Default.USER;// "manager";
                Password = Settings.Default.KEY;//"1234";

                sboCompany.Server = Server;//mandatory property 
                switch (DbServerTypeXML)
                {
                    case " MS 2005":
                        DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                        break;
                    case "MS 2008":
                        DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                        break;
                    case "MS 2012":
                        DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                        break;
                    case "MS 2014":
                        DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                        break;
                    case "MS 2016":
                        DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                        break;
                    case "MS 2017":
                        DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
                        break;
                    case "MS 2019":
                        DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2019;
                        break;
                    case "HANADB":
                        DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                        break;
                }
                sboCompany.DbServerType = DbServerType;
                sboCompany.CompanyDB = CompanyDB;
                sboCompany.UserName = UserName;
                sboCompany.Password = Password;
                //sboCompany.DbUserName = DbUserName;
                //sboCompany.DbPassword = DbPassword;
                sboCompany.UseTrusted = false;
                long errorCode = sboCompany.Connect();
                if (errorCode != 0)
                {
                    //MessageBox.Show(" SIN CONEXION: ");
                    //RegistrarEvento("No se pudo Conectar con el sevidor " + DateTime.Now);
                }
                else
                {
                    //MessageBox.Show(" CONEXION: ");
                }
               
            }
            catch (Exception ex)
            {
                GuardarError("Error conexion :" + ex.Message, "Conexion");
            }
        }

        public void Conectar()
        {
            string sErrMsg = "";                    
            try
            {
                SAPbobsCOM.BoDataServerTypes DbServerType = (SAPbobsCOM.BoDataServerTypes)0;
                string DbServerTypeXML = null;               
                DbServerTypeXML = Settings.Default.TYPOSERVER; //"MS 2014";               

                //if (sboCompany == null)
                //{
                    //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString());
                    sboCompany = new SAPbobsCOM.Company(); //creamos una nueva instacia del objeto company
                    sboCompany.CompanyDB = Settings.Default.DATABASE;
                    sboCompany.Server = Settings.Default.SERVER;                    
                    //oCompany.language = SAPbobsCOM.BoSuppLangs.ln_Spanish;
                    switch (DbServerTypeXML)
                    {
                        case " MS 2005":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                            break;
                        case "MS 2008":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                            break;
                        case "MS 2012":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                            break;
                        case "MS 2014":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                            break;
                        case "MS 2016":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                            break;
                        case "MS 2017":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
                            break;
                        case "MS 2019":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2019;
                            break;
                        case "HANADB":
                            DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                            break;
                    }
                    sboCompany.DbServerType = DbServerType;
                    sboCompany.UserName = Settings.Default.USER; // "manager";
                    sboCompany.Password = Settings.Default.KEY; // "B1Admin";
                                       
                    //oCompany.DbUserName = Properties.Settings.Default.DbUserName; // "SYSTEM";
                    //oCompany.DbPassword = Properties.Settings.Default.DbPassword; // "SAPHANAsna2016";
                    //oCompany.UseTrusted = false;
                    //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString());
                    int returnSAPBO = sboCompany.Connect();
                    //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString());
                    if (returnSAPBO != 0)
                    {
                        sboCompany.GetLastError(out returnSAPBO, out sErrMsg);
                        GuardarError("Error conexion :" + sErrMsg, "Conexion");
                    }
                    else
                    {
                       // GuardarError("Error conexion :" + sErrMsg, "Conexion");  
                    }
                //}
            }
            catch (Exception ex)
            {
                GuardarError("Error conexion :" + ex.Message, "Conexion");
            }
        }

        /// <summary>
        /// InicarSBO
        /// </summary>
        public void InicarSBO()
        {
            if (oCompany.Connected)
            {
                oCompany.Disconnect();
                Conectar();
            }
            else
                Conectar();
        }

        public void DesconectarSBO()
        {
            if (oCompany.Connected)
            {
                oCompany.Disconnect();
            }
        }
        #endregion conexion
        
        /// <summary>
        /// 
        /// </summary>
        /// 
        public void CrearDocuments()
        {
            #region Variables locales
            SAPbobsCOM.Documents Documents = null;
            string sError = null;
            string sError2 = null;
            string sTipoDoc = null;
            string sTipoDoc2 = null;
            string sDocNum = null;
            string sFolioNumberVal = null;
            string sCardCode = null;
            string sCardName = null;
            string sItemDescr = null;
            string sTypeItem = null;
            string sDocCurrency = null;
            string sCard = null;
            string sCard2 = null;
            string sGroupCode = "-1";
            string sFolioNumber = null;
            string TaxCode = null;
            string sNroHabit = null;
            string sNameFile = null;
            string NumberAccount = null;
            string sCostingCode = null;
            string stMontoUSDforDeci = null;
            string sTipoPago = null;
            string sTypeServiceDC = null;
            string sSinNumberAcc = null;
            string FolioPrefixString = null;
            string FolioNumber = null;
            string sRUT = null;
            List<string> lsfilesProcess = new List<string>();
            double dbMontoUSD = 0;
            //double dbMontoCashUSD = 0;
            double dbTransferSumUSD = 0;
            //double dbMontoCash = 0;
            double dbTransferSum = 0;
            double dbRate = 0;
            //double dbAmountCLPDC = 0;
            //double dbAmounTUSDDC = 0;
            double dbMontoCLP = 0;
            int inDocReg = 0;
            int iDesde = 0;
            int iHasta = 0;
            int i = 0;
            DateTime dtDocDueDate = new DateTime();
            DateTime dtTaxDate = new DateTime();
            string[] lsfiles = null;
            string[] lsfilesAr = null;
            List<string> lsfiles2 = new List<string>();// null;

            bool SearchDoc = false;
            List<Paymenttype> lsPay = new List<Paymenttype>();
            Paymenttype pay = new Paymenttype();
            #endregion Variables locales

            try
            {
                System.IO.StreamReader streamLectura;
                string sFiles = "";
                try
                {
                    lsfiles = System.IO.Directory.GetFiles(sPathExcel + @"\" + sFiles, "*.txt");
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                }

                try
                {
                    if (!System.IO.Directory.Exists(sPathExcel + @"\" + "Procesados"))
                    {
                        System.IO.Directory.CreateDirectory(sPathExcel + @"\" + "Procesados");
                    }
                }
                catch (Exception)
                {
                    
                }
                try
                {
                    if (!System.IO.Directory.Exists(sPathExcel + @"\" + "Log"))
                    {
                        System.IO.Directory.CreateDirectory(sPathExcel + @"\" + "Log");
                    }
                }
                catch (Exception)
                {
                    
                }

                foreach (string sFileName1 in lsfiles)
                {
                    sNameFile = System.IO.Path.GetFileName(sFileName1);
                    try
                    {
                        System.IO.File.Copy(sFileName1, sPathExcel + @"\Procesados\" + sNameFile);
                    }
                    catch
                    { }
                }
                if (lsfiles.Length > 0)
                {
                    string sFile1 = null;
                    foreach (string sFile11 in lsfiles)
                    {
                        sFile1 = System.IO.Path.GetFileName(sFile11);
                        lsfilesAr = System.IO.Directory.GetFiles(sPathExcel + @"\Procesados\", sFile1);
                        lsfiles2.Add(lsfilesAr[0].ToString());
                    }
                }
                else
                    lsfiles2 = null;
                foreach (string sFileName in lsfiles2)
                {
                    #region ForEach
                    sNameFile = System.IO.Path.GetFileName(sFileName);
                    try
                    {
                        System.IO.File.Delete(sPathExcel + @"\" + sNameFile);
                    }
                    catch
                    {
                    }

                    streamLectura = new System.IO.StreamReader(sFileName, Encoding);
                    i = 1;
                    string str_Linea = streamLectura.ReadLine();

                    #region While
                    while (str_Linea != null)
                    {
                        try
                        {
                            #region Lineas TXT
                            iDesde = str_Linea.IndexOf(";");
                            sCardCode = str_Linea.Substring(0, (iDesde));
                            sRUT = sCardCode;
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //sCardCode = str_Linea.Substring(0, (iDesde));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            sCardName = str_Linea.Substring(0, (iDesde));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            // sCardName = str_Linea.Substring(0, (iDesde));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            sTipoDoc = str_Linea.Substring(0, (iDesde));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //sTipoDoc = str_Linea.Substring(0, (iDesde));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            sFolioNumber = str_Linea.Substring(0, (iDesde));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            dtTaxDate = Convert.ToDateTime(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            dtDocDueDate = Convert.ToDateTime(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dtDocDueDate = Convert.ToDateTime(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dtDocDueDate = Convert.ToDateTime(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            sTipoPago = Convert.ToString(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dtDocDueDate = Convert.ToDateTime(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            sTypeItem = (str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            sItemDescr = (str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));

                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;
                            //dbRate = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));  //tipo de cambio1
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            dbRate = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));//tipo de cambio2
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //sItemDescr = (str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //sItemDescr = (str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //sItemDescr = (str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            sDocCurrency = (str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSDforDeci = Convert.ToDouble((str_Linea.Substring(0, (iDesde))).ToString().Replace(".", ","));
                            stMontoUSDforDeci = Convert.ToString((str_Linea.Substring(0, (iDesde))).ToString());
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            dbMontoCLP = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            sDocNum = (str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            sNroHabit = Convert.ToString(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            //dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            ////dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            //str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            //iHasta = str_Linea.IndexOf(";");
                            //iDesde = iHasta;

                            ////dbMontoUSD = Convert.ToDouble(str_Linea.Substring(0, (iDesde)));
                            //str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            //iHasta = str_Linea.IndexOf(";");
                            //iDesde = iHasta;

                            sCard = Convert.ToString(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;

                            sCard2 = Convert.ToString(str_Linea.Substring(0, (iDesde)));
                            str_Linea = str_Linea.Substring(iDesde + 1, (str_Linea.Length - (iDesde + 1)));
                            iHasta = str_Linea.IndexOf(";");
                            iDesde = iHasta;
                            #endregion Lineas TXT
                        }
                        catch (Exception ex)
                        {
                            GuardarError(" while Exception Folio  :" + sFolioNumber.Trim() + " Error :" + ex.Message + " " + ex.InnerException, sFolioNumber);
                            //paso en caso de Error
                            //str_Linea = streamLectura.ReadLine();
                            //i++;
                            sFolioNumber = null;
                        }
                    Begin:
                        {
                            string sFolioP = null, sFolioN = null;
                            //GuardarError(" Folio Leido : " + sFolioNumber, sNameFile);

                            var sFolioArr = sFolioNumber.Split('-');
                            try { sFolioP = sFolioArr[0]; }
                            catch { sFolioP = null; }

                            try { sFolioN = sFolioArr[1]; }
                            catch { sFolioN = null; }

                            //GuardarError(" Folio Prefijo : " + sFolioP + " , Folio Numero :" + sFolioN, sNameFile);
                            if (!string.IsNullOrEmpty(sFolioNumber.Replace("-", "").Trim()) && !string.IsNullOrEmpty(sFolioP) && !string.IsNullOrEmpty(sFolioN))
                            {

                                //GuardarError(" Folio Prefijo : " + sFolioP + " , Folio Numero :" + sFolioN + " Sigue con el proceso normal", sNameFile);
                                //MessageBox.Show(" Sigue con el proceso normal ");
                                if (dbMontoCLP != 0)
                                {
                                    FolioPrefixString = sFolioNumber.Trim().Substring(0, sFolioNumber.Trim().IndexOf("-"));
                                    FolioNumber = (sFolioNumber.Trim().Substring((sFolioNumber.Trim().IndexOf("-") + 1), (sFolioNumber.Trim().Length - (sFolioNumber.Trim().IndexOf("-") + 1))));
                                    if (FolioPrefixString.Trim().Equals("61") && string.IsNullOrEmpty(sTipoDoc))
                                    {
                                        sTipoDoc = "NCB";
                                        sCardCode = "CBOLETA";
                                    }
                                    else if (sTipoDoc.Trim().Equals("NCE"))
                                    {
                                        sCardCode = "XPASEXT";
                                        sCardName = "PASANTE EXTRANJERO";
                                    }
                                    SearchDoc = this.SearchDocument(sTipoDoc.Trim(), sFolioNumber);
                                    if (SearchDoc)
                                    {
                                        if (sFolioNumber.Trim() == sFolioNumberVal || i == 1)
                                        {

                                            sTipoDoc2 = sTipoDoc;
                                            if (string.IsNullOrEmpty(sFolioNumberVal))
                                            {
                                                if (sTipoDoc.Trim().Equals("BO") || sTipoDoc.Trim().Equals("NCB"))
                                                    sCardCode = "CBOLETA";
                                                else if (sTipoDoc.Trim().Equals("FE") || sTipoDoc.Trim().Equals("NCE"))
                                                {
                                                    sCardCode = "55.555.555-5";
                                                    sCardCode = this.BuscarSN(sCardCode.Trim(), sCardName, sGroupCode, sFiles);
                                                    sCardName = "PASANTE EXTRANJERO";
                                                }
                                                else
                                                {
                                                    sCardCode = this.BuscarSN(sCardCode.Trim(), sCardName, sGroupCode, sFiles);
                                                }
                                                DesconectarSBO();
                                                InicarSBO();
                                                #region Head
                                                if (!string.IsNullOrEmpty(sCardCode))
                                                {

                                                    switch (sTipoDoc.Trim())
                                                    {
                                                        #region FN oInvoices
                                                        case "FN":
                                                            try
                                                            {
                                                                Documents = (SAPbobsCOM.Documents)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices));
                                                                Documents.CardCode = sCardCode.Trim();
                                                                Documents.CardName = sCardName;
                                                                Documents.DocCurrency = "$"; // Peso $
                                                                Documents.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service;
                                                                Documents.FolioPrefixString = sFolioNumber.Trim().Substring(0, sFolioNumber.Trim().IndexOf("-"));
                                                                Documents.FolioNumber = int.Parse(sFolioNumber.Trim().Substring((sFolioNumber.Trim().IndexOf("-") + 1), (sFolioNumber.Trim().Length - (sFolioNumber.Trim().IndexOf("-") + 1))));
                                                                Documents.NumAtCard = sDocNum.Trim();
                                                                Documents.Comments = "Opera : ";
                                                                Documents.JournalMemo = "Servicio de Alojamiento Nro. Habitación :" + sNroHabit;
                                                                sFolioNumberVal = sFolioNumber.Trim();
                                                                dbTransferSum = 0;
                                                                dbTransferSumUSD = 0;
                                                            }
                                                            catch (Exception)
                                                            {
                                                                
                                                            }
                                                            break;
                                                        #endregion FN oInvoices
                                                        #region NC and NCB oCreditNotes
                                                        case "NC":
                                                        case "NCB":
                                                            try
                                                            {
                                                                Documents = (SAPbobsCOM.Documents)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes));
                                                                Documents.CardCode = sCardCode.Trim();
                                                                Documents.CardName = sCardName;
                                                                Documents.DocDate = dtTaxDate;
                                                                Documents.TaxDate = dtTaxDate;
                                                                Documents.DocCurrency = "$"; // Peso $
                                                                Documents.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service;
                                                                Documents.FolioPrefixString = sFolioNumber.Trim().Substring(0, sFolioNumber.Trim().IndexOf("-"));
                                                                Documents.FolioNumber = int.Parse(sFolioNumber.Trim().Substring((sFolioNumber.Trim().IndexOf("-") + 1), (sFolioNumber.Trim().Length - (sFolioNumber.Trim().IndexOf("-") + 1))));
                                                                Documents.NumAtCard = sDocNum.Trim();
                                                                Documents.Comments = "Opera";
                                                                Documents.JournalMemo = "Servicio de Alojamiento Nro. Habitación :" + sNroHabit;
                                                                Documents.Indicator = "61";
                                                                if (sTipoDoc.Trim().Equals("NCB"))
                                                                    Documents.FederalTaxID = sRUT.Trim().Replace(".", "");

                                                                sFolioNumberVal = sFolioNumber.Trim();
                                                                //dbMontoCash = 0;
                                                                //dbMontoCashUSD = 0;
                                                                dbTransferSum = 0;
                                                                dbTransferSumUSD = 0;
                                                            }
                                                            catch (Exception)
                                                            {

                                                            }
                                                            break;
                                                        #endregion NC oCreditNotes
                                                        #region NC oCreditNotes Export
                                                        case "NCE":
                                                            try
                                                            {
                                                                Documents = (SAPbobsCOM.Documents)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes));
                                                                Documents.CardCode = sCardCode.Trim();
                                                                Documents.CardName = sCardName;
                                                                Documents.DocDate = dtTaxDate;
                                                                Documents.TaxDate = dtTaxDate;
                                                                Documents.DocRate = Math.Round(dbRate, 2);
                                                                Documents.DocCurrency = "US$"; // Dolar Americano US$
                                                                Documents.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service;
                                                                Documents.FolioPrefixString = sFolioNumber.Trim().Substring(0, sFolioNumber.Trim().IndexOf("-"));
                                                                Documents.FolioNumber = int.Parse(sFolioNumber.Trim().Substring((sFolioNumber.Trim().IndexOf("-") + 1), (sFolioNumber.Trim().Length - (sFolioNumber.Trim().IndexOf("-") + 1))));
                                                                Documents.NumAtCard = sDocNum.Trim();
                                                                Documents.Comments = "Opera";
                                                                Documents.JournalMemo = "Servicio de Alojamiento Nro. Habitación :" + sNroHabit;
                                                                Documents.Indicator = "12";
                                                                sFolioNumberVal = sFolioNumber.Trim();
                                                                //dbMontoCash = 0;
                                                                //dbMontoCashUSD = 0;
                                                                dbTransferSum = 0;
                                                                dbTransferSumUSD = 0;
                                                            }
                                                            catch (Exception)
                                                            {
                                                             
                                                            }
                                                            break;
                                                        #endregion NC oCreditNotes Export
                                                        #region BO bod_Bill
                                                        case "BO":
                                                            try
                                                            {
                                                                Documents = (SAPbobsCOM.Documents)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices));
                                                                Documents.DocumentSubType = SAPbobsCOM.BoDocumentSubType.bod_Bill;
                                                                Documents.CardCode = sCardCode;
                                                                Documents.CardName = sCardName;
                                                                Documents.DocDate = dtTaxDate;
                                                                Documents.TaxDate = dtTaxDate;
                                                                Documents.DocCurrency = "$"; // Peso $
                                                                Documents.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service;
                                                                Documents.FolioPrefixString = sFolioNumber.Trim().Substring(0, sFolioNumber.Trim().IndexOf("-"));
                                                                Documents.FolioNumber = int.Parse(sFolioNumber.Trim().Substring((sFolioNumber.Trim().IndexOf("-") + 1), (sFolioNumber.Trim().Length - (sFolioNumber.Trim().IndexOf("-") + 1))));
                                                                Documents.NumAtCard = sDocNum.Trim();
                                                                Documents.Comments = "Opera";
                                                                Documents.JournalMemo = "Servicio de Alojamiento Nro. Habitación :" + sNroHabit;
                                                                sFolioNumberVal = sFolioNumber.Trim();
                                                                //dbMontoCash = 0;
                                                                //dbMontoCashUSD = 0;
                                                                dbTransferSum = 0;
                                                                dbTransferSumUSD = 0;
                                                            }
                                                            catch (Exception)
                                                            {
                                                                //RegistrarEvento("Error en el Folio :" + sFolioNumber.Trim() + " Error :" + ex.Message + " " + ex.InnerException);
                                                            }
                                                            break;
                                                        #endregion BO
                                                        #region  FE bod_ExportInvoice
                                                        case "FE":
                                                            try
                                                            {
                                                                Documents = (SAPbobsCOM.Documents)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices));
                                                                Documents.DocumentSubType = SAPbobsCOM.BoDocumentSubType.bod_ExportInvoice;
                                                                Documents.CardCode = sCardCode.Trim();
                                                                Documents.CardName = sCardName;
                                                                Documents.DocDate = dtTaxDate;
                                                                Documents.TaxDate = dtTaxDate;
                                                                Documents.DocRate = Math.Round(dbRate, 2);
                                                                Documents.DocCurrency = "US$"; // Dolar Americano US$
                                                                Documents.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service;
                                                                Documents.FolioPrefixString = sFolioNumber.Trim().Substring(0, sFolioNumber.Trim().IndexOf("-"));
                                                                Documents.FolioNumber = int.Parse(sFolioNumber.Trim().Substring((sFolioNumber.Trim().IndexOf("-") + 1), (sFolioNumber.Trim().Length - (sFolioNumber.Trim().IndexOf("-") + 1))));
                                                                Documents.NumAtCard = sDocNum.Trim();
                                                                Documents.Comments = "Opera";
                                                                Documents.JournalMemo = "Servicio de Alojamiento Nro. Habitación :" + sNroHabit;
                                                                sFolioNumberVal = sFolioNumber.Trim();
                                                                //dbMontoCash = 0;
                                                                //dbMontoCashUSD = 0;
                                                                dbTransferSum = 0;
                                                                dbTransferSumUSD = 0;
                                                            }
                                                            catch (Exception)
                                                            {
                                                                //RegistrarEvento("Error en el Folio :" + sFolioNumber.Trim() + " Error :" + ex.Message + " " + ex.InnerException);
                                                            }
                                                            break;
                                                        #endregion FE bod_ExportInvoice
                                                        default:
                                                            SearchDoc = false;
                                                            break;
                                                    }
                                                }
                                                else
                                                    i = 0;
                                                #endregion Head
                                            }
                                            #region Lines
                                            NumberAccount = this.NumberAcount(sTypeItem.Trim().ToString(), sNameFile, sDocNum);
                                            if (string.IsNullOrEmpty(NumberAccount))
                                                sSinNumberAcc = "S";
                                            switch (sTipoPago.Trim())
                                            {
                                                case "89":
                                                case "78":
                                                    break;
                                                #region Pagos
                                                case "80":
                                                case "90":
                                                case "95":
                                                    if (sTypeItem.Trim() == "9070" || sTypeItem.Trim() == "8000" || sTypeItem.Trim() == "9028")
                                                    {
                                                        if (sTypeItem.Trim() == "9070")
                                                            UpdateBP(sCardCode);// no se pagan
                                                    }
                                                    else if (sTypeItem.Trim() == "9028") // diferencia de cambio
                                                    {
                                                        //// si la diferencia de cambio es negativa se le envia para se suma al pago y queda a pago a favor del clinete 
                                                        //// pay 99 y inv 100
                                                        //// pay  dbMontoCLP > 0 && 
                                                        //if ((sTipoDoc.Trim() != "NCE" || sTipoDoc.Trim() != "NC"))
                                                        //{
                                                        //    if (sTipoDoc.Trim() == "NCE" || sTipoDoc.Trim() == "NC")
                                                        //        dbTransferSumUSD = (Convert.ToDouble(stMontoUSDforDeci.Replace(".", "")) / 10000) * (-1);
                                                        //    else
                                                        //        dbTransferSumUSD = Convert.ToDouble(stMontoUSDforDeci.Replace(".", "")) / 10000;

                                                        //    if (sTipoDoc.Trim() == "NCE" || sTipoDoc.Trim() == "NC")
                                                        //        dbTransferSum = dbMontoCLP * (-1);
                                                        //    else
                                                        //        dbTransferSum = dbMontoCLP;

                                                        //    dbAmountCLPDC = dbTransferSum;
                                                        //    dbAmounTUSDDC = dbTransferSumUSD;
                                                        //    sTypeServiceDC = sTypeItem.Trim();
                                                        //}
                                                    }
                                                    else
                                                    {
                                                        if (sTipoDoc.Trim() == "NCE" || sTipoDoc.Trim() == "NC" || sTipoDoc.Trim().Equals("NCB"))
                                                            dbTransferSumUSD = (Convert.ToDouble(stMontoUSDforDeci.Replace(".", "")) / 10000) * (-1);
                                                        else
                                                            dbTransferSumUSD = Convert.ToDouble(stMontoUSDforDeci.Replace(".", "")) / 10000;

                                                        if (sTipoDoc.Trim() == "NCE" || sTipoDoc.Trim() == "NC" || sTipoDoc.Trim().Equals("NCB"))
                                                            dbTransferSum = dbMontoCLP * (-1);
                                                        else
                                                            dbTransferSum = dbMontoCLP;

                                                        pay.dbAmountUSD = Math.Round(dbTransferSumUSD, 2);
                                                        pay.dbAmountCPL = dbTransferSum;
                                                        pay.sCurrency = sDocCurrency.Trim();
                                                        pay.sType = "T";
                                                        pay.sTypeService = sTypeItem.Trim();
                                                        pay.sItemDescr = sItemDescr;
                                                        pay.dbRate = dbRate;
                                                        pay.sCard = sCard;
                                                        pay.sCardUSD = sCard2;
                                                        lsPay.Add(pay);
                                                    }
                                                #endregion Pagos
                                                    break;
                                                
                                                    #region Servicios
                                                default:
                                                    if (!string.IsNullOrEmpty(NumberAccount))
                                                    {
                                                        try
                                                        {
                                                            sCostingCode = NumberAccount;
                                                            NumberAccount = NumberAccount.Substring(0, NumberAccount.IndexOf("|"));
                                                            iDesde = sCostingCode.IndexOf("|");
                                                            sCostingCode = sCostingCode.Substring(iDesde + 1, (sCostingCode.Length - (iDesde + 1)));
                                                            iHasta = sCostingCode.IndexOf("|");
                                                            iDesde = iHasta;
                                                            TaxCode = (sCostingCode.Substring(0, (iDesde)));
                                                            sCostingCode = sCostingCode.Substring(iDesde + 1, (sCostingCode.Length - (iDesde + 1)));
                                                            switch (sTipoDoc.Trim())
                                                            {
                                                                case "FE":
                                                                    Documents.Lines.ItemDescription = sTypeItem.Trim() + " " + sItemDescr;
                                                                    Documents.Lines.AccountCode = NumberAccount;
                                                                    if (sDocCurrency.Trim() == "CLP")
                                                                    {
                                                                        Documents.Lines.UnitPrice = dbMontoCLP;
                                                                        Documents.Lines.Currency = "$"; // Precio en Peso $
                                                                    }
                                                                    else
                                                                    {
                                                                        Documents.Lines.UnitPrice = Convert.ToDouble(stMontoUSDforDeci.Replace(".", "")) / 10000;
                                                                        Documents.Lines.Rate = Math.Round(dbRate, 2); 
                                                                        Documents.Lines.Currency = "US$"; // Precio Dolar Americano US$
                                                                    }
                                                                    Documents.Lines.TaxCode = (sTipoDoc.Trim() == "FE" ? "IVA_EXE" : TaxCode);
                                                                    Documents.Lines.CostingCode = sCostingCode;
                                                                    Documents.Lines.Add();
                                                                    break;
                                                                case "NCE":
                                                                    Documents.Lines.ItemDescription = sTypeItem.Trim() + " " + sItemDescr;
                                                                    Documents.Lines.AccountCode = NumberAccount;
                                                                    if (sDocCurrency.Trim() == "CLP")
                                                                    {
                                                                        Documents.Lines.UnitPrice = dbMontoCLP * (-1);
                                                                        Documents.Lines.Currency = "$"; // Precio en Peso $
                                                                    }
                                                                    else
                                                                    {
                                                                        Documents.Lines.UnitPrice = (Convert.ToDouble(stMontoUSDforDeci.Replace(".", "")) / 10000) * (-1);
                                                                        Documents.Lines.Rate = Math.Round(dbRate, 2);
                                                                        Documents.Lines.Currency = "US$"; // Precio Dolar Americano US$
                                                                    }
                                                                    Documents.Lines.TaxCode = (sTipoDoc.Trim() == "NCE" ? "IVA_EXE" : TaxCode);
                                                                    Documents.Lines.CostingCode = sCostingCode;
                                                                    Documents.Lines.Add();
                                                                    break;
                                                                case "NC":
                                                                case "NCB":
                                                                    Documents.Lines.ItemDescription = sTypeItem.Trim() + " " + sItemDescr;
                                                                    Documents.Lines.AccountCode = NumberAccount;
                                                                    Documents.Lines.LineTotal = dbMontoCLP * (-1);
                                                                    Documents.Lines.Currency = "$";// Pesos Chilenos
                                                                    Documents.Lines.TaxCode = (sTipoDoc.Trim() == "FE" ? "IVA_EXE" : TaxCode);
                                                                    Documents.Lines.CostingCode = sCostingCode;
                                                                    Documents.Lines.Add();
                                                                    break;
                                                                case "FN":
                                                                    Documents.Lines.ItemDescription = sTypeItem.Trim() + " " + sItemDescr;
                                                                    Documents.Lines.AccountCode = NumberAccount;
                                                                    Documents.Lines.LineTotal = dbMontoCLP;
                                                                    Documents.Lines.Currency = "$";// Pesos Chilenos
                                                                    Documents.Lines.TaxCode = (sTipoDoc.Trim() == "FE" ? "IVA_EXE" : TaxCode);
                                                                    Documents.Lines.CostingCode = sCostingCode;
                                                                    Documents.Lines.Add();
                                                                    break;
                                                                case "BO":
                                                                    Documents.Lines.ItemDescription = sTypeItem.Trim() + " " + sItemDescr;
                                                                    Documents.Lines.AccountCode = NumberAccount;
                                                                    Documents.Lines.LineTotal = dbMontoCLP;
                                                                    Documents.Lines.Currency = "$";// Pesos Chilenos
                                                                    Documents.Lines.TaxCode = (TaxCode.Trim() == "IVA" ? "IVA_BOL" : "IVA_EXE");
                                                                    Documents.Lines.CostingCode = sCostingCode;
                                                                    Documents.Lines.Add();
                                                                    break;
                                                            }
                                                            Documents.DocDate = dtTaxDate;
                                                            Documents.TaxDate = dtTaxDate;
                                                        }
                                                        catch (Exception)
                                                        {
                                                            //RegistrarEvento("Error al add Linea del Folio :" + sFolioNumber.Trim() + " Error al add Line error :" + ex.Message

                                                            //RegistrarEvento(" " + ex.InnerException);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //RegistrarEvento("Error servicio no tiene cuenta asociada :" + sItemDescr);
                                                    }
                                                #endregion Servicios

                                                    break;
                                            }
                                            #endregion Lines
                                        }
                                        else
                                        {
                                            #region Creacion del documento
                                            try
                                            {
                                                if (sSinNumberAcc != "S")
                                                {
                                                    InicarSBO();
                                                    inDocReg = Documents.Add();
                                                    if (inDocReg != 0)
                                                    {//7
                                                        sError = sboCompany.GetLastErrorDescription();
                                                        sError2 = "Error 7 al registrar el Documento :" + sDocNum.Trim() + " Folio :" + sFolioNumber.Trim() + " Error :" + sError + " Archivo :" + sNameFile + " Fecha :" + DateTime.Now;

                                                        GuardarError(sError2, sNameFile);
                                                    }
                                                    else
                                                    {
                                                        GuardarError("1 Documento creado exitosamente Folio :" + sFolioNumber.Trim(), sNameFile);
                                                        CreatePayments(lsPay, Documents.FolioPrefixString, Documents.FolioNumber, Documents.CardCode, Documents.CardName, Documents.TaxDate, Documents.DocDate, sTipoDoc2.Trim()/*, dbAmountCLPDC*/, sTypeServiceDC/*, dbAmounTUSDDC*/);
                                                    }
                                                    i = 1;
                                                    Documents = null;
                                                    sFolioNumberVal = null;
                                                    dbTransferSum = 0;
                                                    dbTransferSumUSD = 0;
                                                    sTypeServiceDC = null;
                                                    sSinNumberAcc = null;
                                                    lsPay.Clear();
                                                    GC.Collect();
                                                    goto Begin;
                                                }
                                                sSinNumberAcc = null;
                                                goto Begin;
                                            }
                                            catch (Exception ex)
                                            {
                                                //10
                                                DesconectarSBO();
                                                sError2 = "Error 10 al registrar el Documento :" + sDocNum.Trim() + " Folio :" + sFolioNumber.Trim() + " Error :" + sError + ex.Message + " Archivo :" + sNameFile + " Fecha :" + DateTime.Now;

                                                GuardarError(sError2, sNameFile);
                                                GC.Collect();
                                            }
                                            finally
                                            {
                                                DesconectarSBO();
                                                GC.Collect();
                                            }
                                            #endregion Creacion del documento
                                        }
                                    }
                                }
                            }
                        }

                        if (i == 1 && SearchDoc == false)
                            i = 0;
                        str_Linea = streamLectura.ReadLine();
                        i++;
                        //GuardarError("Siguiente linea" + str_Linea, sNameFile);
                    }// Next Line

                    #endregion While

                    #region Creacion del documento
                    try
                    {
                        if (sSinNumberAcc != "S")
                        {
                            //MessageBox.Show(" sSinNumberAcc != S ");
                            InicarSBO();
                            //MessageBox.Show(" conectado ");
                            inDocReg = Documents.Add();
                            //MessageBox.Show(" enviando doc ");
                            if (inDocReg != 0)
                            {
                                //9
                                sError = sboCompany.GetLastErrorDescription();
                                sError2 = "Error al 9 registrar el Folio :" + sFolioNumber.Trim() + "Error al registrar el Documento :" + sDocNum.Trim() + " Error :" + sError + " Archivo :" + sNameFile + " Fecha :" + DateTime.Now;
                                GuardarError(sError2, sNameFile);
                            }
                            else
                            {
                                //GuardarError("2 Documento creado exitosamente Folio :" + sFolioNumber.Trim(),sFolioNumber );
                                CreatePayments(lsPay, Documents.FolioPrefixString, Documents.FolioNumber, Documents.CardCode, Documents.CardName, Documents.TaxDate, Documents.DocDate, sTipoDoc2.Trim()/*, dbAmountCLPDC*/, sTypeServiceDC/*, dbAmounTUSDDC*/);
                            }
                            i = 1;
                            Documents = null;
                            sFolioNumberVal = null;
                            dbTransferSum = 0;
                            dbTransferSumUSD = 0;
                            sTypeServiceDC = null;
                            sSinNumberAcc = null;
                            lsPay.Clear();
                        }
                        sSinNumberAcc = null;
                        //goto Begin;
                    }
                    catch (Exception ex)
                    {
                        //8
                        DesconectarSBO();
                        sError2 = "Error 8 al registrar el Documento :" + sDocNum.Trim() + " Folio :" + sFolioNumber.Trim() + " Error :" + sError + ex.Message + " Archivo :" + sNameFile + " Fecha :" + DateTime.Now;
                        GuardarError("" +sError2 + ex.Message, sNameFile);
                    }
                    finally
                    {
                        DesconectarSBO();
                        GC.Collect();
                    }
                    #endregion Creacion del documento

                    lsfilesProcess.Add(sFileName);

                    #endregion ForEach
                }// Next file


            }
            catch (Exception ex)
            {
                GuardarError("Error Crear Documents :" + ex.Message, sNameFile);
            }
        }
        
        public void CreatePayments(List<Paymenttype> lsPay, string FolioPrefixString, int FolioNumber, string sCardCode, string sCardName, DateTime dtTaxDate, DateTime
                                   dtDocDueDate, string sTipoDoc, string sTypeServiceDC)
        {
            int i = 0;
            foreach (Paymenttype pay in lsPay)
            {
                i++;
                string sNumberAcount = this.NumberAcountPay(pay.sTypeService);
                string NumberAcount = null;
                double CashAmount = 0;
                double CashAmountCLP = 0;
                double CashAmountUSD = 0;
                double dbAmountUSD = 0;
                double dbAmountCLP = 0;
                decimal Saldo = 0;
                decimal TotalPay2 = 0;
                string sTipoPago = null;
                string NumberAcountDC = null;
                string DocEntry = null;
                string sJournalRemarks = null;
                int iDesde = 0;
                NumberAcount = sNumberAcount.Substring(0, sNumberAcount.IndexOf("|"));
                iDesde = sNumberAcount.IndexOf("|");
                sTipoPago = sNumberAcount.Substring(iDesde + 1, (sNumberAcount.Length - (iDesde + 1)));
                DocEntry = Convert.ToString(this.SearchDocument(sTipoDoc, FolioPrefixString, FolioNumber.ToString()));
                SAPbobsCOM.Payments oPayments = null;
                try
                {
                    if (sCardCode == "55.555.555-5")
                        sCardCode = "XPASEXT";
                    if (sTipoDoc.Trim() == "BO" || sTipoDoc.Trim().Equals("NCB"))
                        sCardCode = "CBOLETA";
                    InicarSBO();
                    switch (sTipoDoc.Trim())
                    {
                        case "NCE":
                        case "NC":
                            oPayments = (SAPbobsCOM.Payments)sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments);
                            oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_OutgoingPayments;
                            oPayments.DocType = SAPbobsCOM.BoRcptTypes.rCustomer;
                            oPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_CredItnote;
                            break;
                        case "NCB":
                            oPayments = (SAPbobsCOM.Payments)sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments);
                            oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_OutgoingPayments;
                            oPayments.DocType = SAPbobsCOM.BoRcptTypes.rCustomer;
                            oPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_CredItnote;
                            break;
                        case "FN":
                        case "BO":
                        case "FE":
                            oPayments = (SAPbobsCOM.Payments)sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments);
                            oPayments.DocObjectCode = SAPbobsCOM.BoPaymentsObjectType.bopot_IncomingPayments;
                            oPayments.DocType = SAPbobsCOM.BoRcptTypes.rCustomer;
                            oPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_Invoice;
                            break;
                    }
                    oPayments.CardCode = sCardCode.Trim();
                    oPayments.CardName = sCardName;
                    oPayments.DocDate = dtTaxDate;
                    oPayments.DueDate = dtTaxDate;
                    oPayments.TaxDate = dtTaxDate;
                    oPayments.CounterReference = FolioNumber.ToString();
                    sJournalRemarks = (pay.sTypeService.TrimEnd() + " " + pay.sItemDescr.TrimEnd() + "/" + FolioPrefixString.TrimEnd() + "-" + FolioNumber.ToString().TrimEnd()).Trim();
                    sJournalRemarks = (sJournalRemarks.Length > 50 ? sJournalRemarks.Substring(0, 49) : sJournalRemarks);
                    oPayments.JournalRemarks = sJournalRemarks;
                    oPayments.Invoices.DocEntry = int.Parse(DocEntry);
                    oPayments.TransferDate = dtTaxDate;
                    oPayments.TransferAccount = NumberAcount;
                    oPayments.TransferReference = (pay.sCurrency == "USD" ? pay.sCardUSD.Trim() : pay.sCard.Trim());//pay.sTypeService + " " + pay.sItemDescr.TrimEnd();
                    if (pay.sCurrency == "USD")
                    {
                        oPayments.DocCurrency = "US$";
                        oPayments.TransferSum = Math.Round(pay.dbAmountUSD, 2);
                        oPayments.DocRate = Math.Round(pay.dbRate, 2);
                        switch (sTipoDoc.Trim())
                        {
                            case "FN":
                            case "BO":
                            case "NC":
                            case "NCB":
                                dbAmountCLP = pay.dbAmountCPL;
                                break;
                            case "NCE":
                            case "FE":
                                dbAmountUSD = Math.Round(pay.dbAmountUSD, 2);
                                break;
                        }
                    }
                    else
                    {

                        oPayments.TransferSum = pay.dbAmountCPL;
                        switch (sTipoDoc.Trim())
                        {
                            case "FN":
                            case "BO":
                            case "NC":
                            case "NCB":
                                oPayments.DocCurrency = "$";
                                dbAmountCLP = pay.dbAmountCPL;
                                break;
                            case "NCE":
                            case "FE":
                                oPayments.DocCurrency = "US$";
                                oPayments.LocalCurrency = SAPbobsCOM.BoYesNoEnum.tYES;
                                oPayments.DocRate = Math.Round(pay.dbRate, 2);
                                dbAmountUSD = Math.Round(pay.dbAmountUSD, 2);
                                break;
                        }
                    }
                    CashAmountUSD = 0;
                    CashAmountCLP = 0;
                    if (i == lsPay.Count)
                    {
                        decimal TotalPay = Convert.ToDecimal(this.GetAmountPay(sTipoDoc, FolioPrefixString, FolioNumber.ToString()));
                        switch (sTipoDoc.Trim())
                        {
                            case "FN":
                            case "BO":
                            case "NC":
                            case "NCB":
                                Saldo = TotalPay;
                                break;
                            case "NCE":
                            case "FE":
                                string decimales = TotalPay.ToString().Trim();
                                if (decimales.IndexOf(".") > 0)
                                {
                                    decimales = decimales.Substring(decimales.IndexOf(".") + 1, decimales.Length - decimales.IndexOf(".") - 1);
                                    if (decimales.ToString().Length == 1)
                                        Saldo = (Convert.ToDecimal(TotalPay.ToString().Replace(".", "")) / 10);
                                    else if (decimales.ToString().Length > 1)
                                        Saldo = (Convert.ToDecimal(TotalPay.ToString().Replace(".", "")) / 100);
                                }
                                else
                                    Saldo = TotalPay;
                                break;
                        }
                        if (pay.sCurrency == "USD")
                        {
                            switch (sTipoDoc.Trim())
                            {
                                case "FN":
                                case "BO":
                                case "NC":
                                case "NCB":
                                    TotalPay2 = ((Saldo) - (Convert.ToDecimal(pay.dbAmountCPL))) / (Convert.ToDecimal(pay.dbRate));
                                    break;
                                case "NCE":
                                case "FE":
                                    TotalPay2 = (Saldo) - (Convert.ToDecimal(Math.Round(pay.dbAmountUSD, 2)));
                                    break;
                            }
                        }
                        else
                        {
                            switch (sTipoDoc.Trim())
                            {
                                case "FN":
                                case "BO":
                                case "NC":
                                case "NCB":
                                    TotalPay2 = (Saldo) - (Convert.ToDecimal(pay.dbAmountCPL));

                                    break;
                                case "NCE":
                                case "FE":
                                    TotalPay2 = ((Saldo) * (Convert.ToDecimal(pay.dbRate))) - (Convert.ToDecimal(pay.dbAmountCPL));
                                    if (TotalPay2 > 0)
                                    {
                                        decimal decimales2 = TotalPay2 - (Math.Truncate(TotalPay2));
                                        if (decimales2 >= Convert.ToDecimal(0.5))
                                        {
                                            TotalPay2 = (Math.Truncate(TotalPay2)) + 1;
                                        }
                                        else
                                        {
                                            TotalPay2 = (Math.Truncate(TotalPay2));
                                        }
                                    }
                                    break;
                            }
                        }
                        if (TotalPay2 > 0)
                        {
                            CashAmount = Convert.ToDouble(TotalPay2);
                            NumberAcountDC = this.NumberAcountPay("9028");
                            NumberAcountDC = NumberAcountDC.Substring(0, sNumberAcount.IndexOf("|")); //limpieza de la cuenta
                            switch (sTipoDoc.Trim())
                            {
                                case "FN":
                                case "BO":
                                case "NC":
                                case "NCB":
                                    CashAmountCLP = Convert.ToDouble((Saldo) - (Convert.ToDecimal(pay.dbAmountCPL)));

                                    break;
                                case "NCE":
                                case "FE":
                                    CashAmountUSD = Convert.ToDouble((Saldo) - (Convert.ToDecimal(Math.Round(pay.dbAmountUSD, 2))));
                                    break;
                            }
                            oPayments.CashSum = CashAmount;
                            oPayments.CashAccount = NumberAcountDC;
                        }
                        else if (TotalPay2 < 0)
                        {
                            switch (sTipoDoc.Trim())
                            {
                                case "FN":
                                case "BO":
                                case "NC":
                                case "NCB":
                                    dbAmountCLP = Convert.ToDouble(Saldo);
                                    break;
                                case "NCE":
                                case "FE":
                                    dbAmountUSD = Convert.ToDouble(Saldo);
                                    break;
                            }
                        }
                    }
                    switch (sTipoDoc.Trim())
                    {
                        case "FN":
                        case "BO":
                        case "NC":
                        case "NCB":
                            oPayments.Invoices.SumApplied = dbAmountCLP + CashAmountCLP;
                            break;
                        case "NCE":
                        case "FE":
                            oPayments.Invoices.AppliedFC = dbAmountUSD + CashAmountUSD;
                            break;
                    }
                    int inDocReg = oPayments.Add();
                    if (inDocReg != 0)
                    {
                        string sError = sboCompany.GetLastErrorDescription() + " " + dbAmountUSD.ToString() + " + " + CashAmountUSD.ToString();

                        string sError2 = "Error al pagar docuemento con el Folio :" + FolioPrefixString.Trim() + " Error : " + sError + " Fecha :" + DateTime.Now;
                        
                        GuardarError(sError2, "");
                        DesconectarSBO();
                    }
                    else
                    {
                        DesconectarSBO();
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    DesconectarSBO();
                }
            }
        }

        private string NumberAcount(string sTypeItem, string sFiles, string sDocNm)
        {
            string sNumberAcount = null;
            SAPbobsCOM.Recordset oRecordset = null;
            try
            {
                InicarSBO();
                oRecordset = (SAPbobsCOM.Recordset)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset));
                oRecordset.DoQuery("SELECT U_AcctCode,U_TaxCode,U_OcrCode FROM [@INXACTXSERVICIO] WHERE Code= '" + sTypeItem + "'");
                if (!string.IsNullOrEmpty(oRecordset.Fields.Item("U_AcctCode").Value.ToString()))
                {
                    sNumberAcount = oRecordset.Fields.Item("U_AcctCode").Value.ToString() + "|" + oRecordset.Fields.Item("U_TaxCode").Value.ToString() + "|" + oRecordset.Fields.Item("U_OcrCode").Value.ToString();
                }
                else
                {
                    sNumberAcount = "Cuenta no existe :" + sTypeItem;
                    GuardarError(sNumberAcount + " en el Archivo : " + sFiles + " El Documento : " + sDocNm, sFiles);
                    sNumberAcount = null;
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                DesconectarSBO();
                GC.Collect();
            }
            return sNumberAcount;
        }

        private string NumberAcountPay(string sTypeItem)
        {
            string sNumberAcount = null;
            SAPbobsCOM.Recordset oRecordset = null;
            try
            {
                InicarSBO();
                oRecordset = (SAPbobsCOM.Recordset)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset));
                String sQuery = "SELECT U_AcctCode,U_TipoPago FROM [@INXACTXSERVICIO] WHERE Code = '" + sTypeItem.Trim() + "'";
                oRecordset.DoQuery(sQuery);
                if (!string.IsNullOrEmpty(oRecordset.Fields.Item("U_AcctCode").Value.ToString()))
                {
                    sNumberAcount = oRecordset.Fields.Item("U_AcctCode").Value.ToString() + "|" + oRecordset.Fields.Item("U_TipoPago").Value.ToString();
                }
                else
                {
                    sNumberAcount = "Cuenta no existe :" + sTypeItem;
                    GuardarError(sNumberAcount, "");
                    sNumberAcount = sNumberAcount = "2220101|T"; ;
                }
            }
            catch
            {
                return sNumberAcount = "2220101|T";
            }
            finally
            {
                DesconectarSBO();
                GC.Collect();
            }
            return sNumberAcount;
        }

        private string SearchDocument(string sTipoDoc, string FolioPrefixString, string FolioNumber)
        {
            string sQuery = null;
            InicarSBO();
            SAPbobsCOM.Recordset oRecordset = null;
            try
            {
                oRecordset = (SAPbobsCOM.Recordset)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset));
                switch (sTipoDoc.Trim())
                {
                    case "FE":
                        sQuery = "SELECT DocEntry,PaidFC FROM OINV T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' AND T0.DocSubType ='IX'";
                        break;
                    case "FN":
                        sQuery = "SELECT DocEntry FROM OINV T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' AND T0.DocSubType ='--'";
                        break;
                    case "NC":
                        sQuery = "SELECT DocEntry FROM ORIN T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' ";
                        break;
                    case "NCE":
                        sQuery = "SELECT DocEntry FROM ORIN T0 WHERE T0.CardCode= 'XPASEXT' AND T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' ";
                        break;
                    case "NCB":
                        sQuery = "SELECT DocEntry FROM ORIN T0 WHERE T0.CardCode= 'CBOLETA' AND T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' ";
                        break;
                    case "BO":
                        sQuery = "SELECT DocEntry FROM OINV T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' AND T0.DocSubType ='IB'";
                        break;
                }
                oRecordset.DoQuery(sQuery);
                if (!string.IsNullOrEmpty((oRecordset.Fields.Item("DocEntry").Value.ToString().Trim())))
                    return Convert.ToString(oRecordset.Fields.Item("DocEntry").Value);
                else
                    return null;
            }
            catch
            {
                return null;
            }
            finally
            {
                DesconectarSBO();
            }
        }

        private string GetAmountPay(string sTipoDoc, string FolioPrefixString, string FolioNumber)
        {
            string sQuery = null;
            InicarSBO();
            SAPbobsCOM.Recordset oRecordset = null;
            try
            {
                oRecordset = (SAPbobsCOM.Recordset)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset));
                switch (sTipoDoc.Trim())
                {
                    case "FE":
                        sQuery = "SELECT (DocTotalFC - PaidFC) AS paid FROM OINV T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' AND T0.DocSubType ='IX'";
                        break;
                    case "FN":
                        sQuery = "SELECT (DocTotal - PaidToDate) AS paid FROM OINV T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' AND T0.DocSubType ='--'";
                        break;
                    case "NC":
                        sQuery = "SELECT (DocTotal - PaidToDate) AS paid FROM ORIN T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' ";
                        break;
                    case "NCB":
                        sQuery = "SELECT (DocTotal - PaidToDate) AS paid FROM ORIN T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' ";
                        break;
                    case "NCE":
                        sQuery = "SELECT (DocTotalFC - PaidFC) AS paid FROM ORIN T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' ";
                        break;
                    case "BO":
                        sQuery = "SELECT (DocTotal - PaidToDate) AS paid FROM OINV T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' AND T0.DocSubType ='IB'";
                        break;
                }
                oRecordset.DoQuery(sQuery);
                if (!string.IsNullOrEmpty((oRecordset.Fields.Item("Paid").Value.ToString().Trim())))
                    return Convert.ToString(oRecordset.Fields.Item("Paid").Value);
                else
                    return null;
            }
            catch
            {
                return null;
            }
            finally
            {
                DesconectarSBO();
            }
        }

        private string GetAmountPay2(string sTipoDoc, string FolioPrefixString, string FolioNumber)
        {
            string sQuery = null;
            InicarSBO();
            SAPbobsCOM.Recordset oRecordset = null;
            try
            {
                oRecordset = (SAPbobsCOM.Recordset)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset));
                switch (sTipoDoc.Trim())
                {
                    case "FE":
                        sQuery = "SELECT DocTotalFC FROM OINV T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' AND T0.DocSubType ='IX'";
                        break;
                }
                oRecordset.DoQuery(sQuery);
                if (!string.IsNullOrEmpty((oRecordset.Fields.Item("DocTotalFC").Value.ToString().Trim())))
                    return Convert.ToString(oRecordset.Fields.Item("DocTotalFC").Value);
                else
                    return null;
            }
            catch
            {
                return null;
            }
            finally
            {
                DesconectarSBO();
            }
        }

        private bool SearchDocument(string sTipoDoc, string sFolioNumber)
        {
            string FolioPrefixString = null;
            string FolioNumber = null;
            string sQuery = null;
            InicarSBO();
            SAPbobsCOM.Recordset oRecordset = null;
            try
            {
                FolioPrefixString = sFolioNumber.Trim().Substring(0, sFolioNumber.Trim().IndexOf("-"));
                FolioNumber = (sFolioNumber.Trim().Substring((sFolioNumber.Trim().IndexOf("-") + 1), (sFolioNumber.Trim().Length - (sFolioNumber.Trim().IndexOf("-") + 1))));

                oRecordset = (SAPbobsCOM.Recordset)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset));
                switch (sTipoDoc)
                {
                    case "FE":
                        sQuery = "SELECT COUNT(*) AS CANT FROM OINV T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' AND T0.DocSubType ='IX'";
                        break;
                    case "FN":
                        sQuery = "SELECT COUNT(*) AS CANT FROM OINV T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' AND T0.DocSubType ='--'";
                        break;
                    case "NC":
                    case "NCE":
                    case "NCB":
                        sQuery = "SELECT COUNT(*) AS CANT FROM ORIN T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' ";
                        break;
                    case "BO":
                        sQuery = "SELECT COUNT(*) AS CANT FROM OINV T0 WHERE T0.FolioPref = '" + FolioPrefixString.Trim() + "' AND T0.FolioNum = '" + FolioNumber.Trim() + "' AND T0.DocSubType ='IB'";
                        break;
                }
                oRecordset.DoQuery(sQuery);
                if (Convert.ToInt32(oRecordset.Fields.Item("CANT").Value.ToString().Trim()) == 0)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
            finally
            {
                DesconectarSBO();
            }
        }

        public void RegistrarEvento(string Error)
        {
            //System.Diagnostics.EventLog LogEvento = new System.Diagnostics.EventLog();
            //LogEvento.Source = "SSAPBOIntegrarLog";
            //LogEvento.WriteEntry(Error);
        }

        private void GuardarError(string sError, string sFileName)
        {
            try
            {
                if (!System.IO.File.Exists(sPathExcel + @"\Log\" + "Log-" + sFileName + ".txt"))
                {
                    // Create a file to write to.
                    using (System.IO.StreamWriter sw = System.IO.File.CreateText(sPathExcel + @"\Log\" + "Log-" + sFileName + ".txt"))
                    {
                        sw.WriteLine(sError);
                    }
                }
                else
                {
                    string Archivo = sPathExcel + @"\Log\" + "Log-" + sFileName + "";
                    using (System.IO.StreamWriter w = System.IO.File.AppendText(Archivo))
                    {
                        w.WriteLine(sError);
                    }
                }

            }
            catch (Exception)
            {
                //RegistrarEvento("Error GuardarError :" + ex.Message);
            }
        }

        public string BuscarSN(string sCardCode, string sCardName, string sGroupCode, string sFiles)
        {
            SAPbobsCOM.Recordset oRecordset = null;
            string sCardCode2 = null;
            string sRut = null;
            try
            {
                InicarSBO();
                oRecordset = (SAPbobsCOM.Recordset)(sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset));
                if (sCardCode == "55.555.555-5")
                {
                    sCardCode2 = "XPASEXT";
                    oRecordset.DoQuery("SELECT CardCode FROM OCRD WHERE CardCode = '" + sCardCode2 + "' ");
                    if (!string.IsNullOrEmpty(oRecordset.Fields.Item("CardCode").Value.ToString()))
                    {
                        return oRecordset.Fields.Item("CardCode").Value.ToString();
                    }
                    else
                    {
                        return this.CrearBP(sCardCode.Trim(), sCardName, sGroupCode);
                    }
                }

                sCardCode2 = sCardCode.Trim();
                sRut = sCardCode2.Replace(".", "");
                oRecordset.DoQuery("SELECT CardCode FROM OCRD WHERE LicTradNum = '" + sRut + "' AND QryGroup1 = 'Y' ");
                if (!string.IsNullOrEmpty(oRecordset.Fields.Item(0).Value.ToString()))
                {
                    return oRecordset.Fields.Item("CardCode").Value.ToString();
                }
                else
                {
                    int iHasta = sCardCode.IndexOf("-");
                    sCardCode2 = sCardCode.Substring(0, (iHasta));
                    sCardCode2 = "C" + sCardCode2;
                    sCardCode2 = sCardCode2.Replace(".", "");

                    oRecordset.DoQuery("SELECT CardCode FROM OCRD WHERE CardCode = '" + sCardCode2 + "' ");
                    if (!string.IsNullOrEmpty(oRecordset.Fields.Item("CardCode").Value.ToString()))
                    {
                        return oRecordset.Fields.Item("CardCode").Value.ToString();
                    }
                    else
                    {
                        return this.CrearBP(sCardCode.Trim(), sCardName, sGroupCode);
                    }
                }

            }
            catch
            {
                return null;
            }
            finally
            {
                DesconectarSBO();
            }
        }

        private string CrearBP(string sCardCode, string sCardName, string sGroupCode)
        {
            int nErr = 0;
            string errMsg = null;
            SAPbobsCOM.BusinessPartners vBP = null;
            try
            {
                InicarSBO();
                vBP = (SAPbobsCOM.BusinessPartners)sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                if (sCardCode == "55.555.555-5")
                {
                    vBP.CardCode = "XPASEXT";
                    vBP.CardName = "PASANTE EXTRANJERO";
                    vBP.Currency = "US$";
                    vBP.FederalTaxID = "55555555-5";
                    vBP.Indicator = "13";
                }
                else
                {
                    string sCardCode2 = null;
                    sCardCode2 = sCardCode.Replace(".", "");
                    int iHasta = sCardCode2.IndexOf("-");
                    vBP.CardCode = "C" + sCardCode2.Substring(0, (iHasta));
                    vBP.CardName = sCardName;
                    vBP.Currency = "##";
                    vBP.FederalTaxID = sCardCode2;
                    vBP.Indicator = "33";
                }
                vBP.CardType = SAPbobsCOM.BoCardTypes.cCustomer;
                vBP.CommissionGroupCode = 0;
                vBP.CommissionPercent = 0;
                vBP.CompanyPrivate = SAPbobsCOM.BoCardCompanyTypes.cPrivate;
                vBP.ContactPerson = "";
                vBP.DiscountPercent = 0;
                vBP.VatLiable = SAPbobsCOM.BoVatStatus.vLiable;
                vBP.SubjectToWithholdingTax = BoYesNoNoneEnum.boNO;//SAPbobsCOM.BoYesNoEnum.tNO;
                nErr = vBP.Add();
                if (nErr != 0)
                {
                    errMsg = sboCompany.GetLastErrorDescription();
                    //RegistrarEvento(errMsg);
                    return null;
                }
                else
                {
                    this.GuardarError("Socio de negocio creado " + sCardCode + " no existia.", "");
                    return vBP.CardCode;
                }
            }
            catch (Exception)
            {
                this.GuardarError("Socio de negocio no creado " + sCardCode + " Error." + errMsg, "");
                //RegistrarEvento("Error CrearBP :" + ex.Message);
                return null;
            }
            finally
            {
                DesconectarSBO();
            }

        }

        private string UpdateBP(string sCardCode)
        {
            int nErr = 0;
            string errMsg = null;
            SAPbobsCOM.BusinessPartners vBP = null;
            try
            {
                string sCardCode2 = null;
                //new validacion                    
                sCardCode2 = sCardCode.Replace(".", "");
                int iHasta = sCardCode2.IndexOf("-");
                sCardCode = "C" + sCardCode2.Substring(0, (iHasta));
                InicarSBO();
                vBP = (SAPbobsCOM.BusinessPartners)sboCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                vBP.GetByKey(sCardCode);
                if (vBP.PayTermsGrpCode != 1)
                {
                    vBP.PayTermsGrpCode = 1;
                    nErr = vBP.Update();
                    if (nErr != 0)
                    {
                        errMsg = sboCompany.GetLastErrorDescription();
                        //RegistrarEvento(errMsg);
                        return null;
                    }
                    else
                    {
                        this.GuardarError("Socio de negocio modificado " + sCardCode + ".", "");
                        return vBP.CardCode;
                    }
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
            finally
            {
                GC.Collect();
            }
        }

        private double ConverDoubledeDataSource(object oDBDataSource)
        {
            double num = 0.0;
            System.Globalization.CultureInfo provider = new System.Globalization.CultureInfo("es-CL", false);
            System.Globalization.CultureInfo info2 = new System.Globalization.CultureInfo("es-CL", false);
            try
            {
                SAPbobsCOM.AdminInfo adminInfo = sboCompany.GetCompanyService().GetAdminInfo();
                provider.NumberFormat.NumberDecimalSeparator = adminInfo.DecimalSeparator;
                provider.NumberFormat.NumberGroupSeparator = adminInfo.ThousandsSeparator;
                info2.NumberFormat.NumberDecimalSeparator = ",";
                info2.NumberFormat.NumberGroupSeparator = ".";
                num = double.Parse(Convert.ToString(oDBDataSource, info2), info2);

                Convert.ToDouble(oDBDataSource);
            }
            catch (Exception)
            {
                try
                {
                    num = double.Parse(Convert.ToString(oDBDataSource, provider), provider);
                }
                catch
                {
                }
            }
            return num;
        }

        private double ConvertToDoubleFromDataSource(object oDBDataSource)
        {
            double num = 0.0;
            System.Globalization.CultureInfo provider = new System.Globalization.CultureInfo("es-VE", false);
            System.Globalization.CultureInfo info2 = new System.Globalization.CultureInfo("es-VE", false);
            try
            {
                SAPbobsCOM.AdminInfo adminInfo = sboCompany.GetCompanyService().GetAdminInfo();
                provider.NumberFormat.NumberDecimalSeparator = adminInfo.DecimalSeparator;
                provider.NumberFormat.NumberGroupSeparator = adminInfo.ThousandsSeparator;
                info2.NumberFormat.NumberDecimalSeparator = ".";
                info2.NumberFormat.NumberGroupSeparator = ",";
                num = double.Parse(Convert.ToString(oDBDataSource, info2), info2);

                Convert.ToDouble(oDBDataSource);
            }
            catch (Exception)
            {
                try
                {
                    num = double.Parse(Convert.ToString(oDBDataSource, provider), provider);
                }
                catch
                {
                }
            }
            return num;
        }

        private string ConvertirDoubleDBDataSource(object oDBDataSource)
        {
            string str = "0";
            try
            {
                System.Globalization.CultureInfo info = new System.Globalization.CultureInfo("es-CL", false);
                System.Globalization.CultureInfo provider = new System.Globalization.CultureInfo("es-CL", false);
                int digits = 0;
                try
                {
                    SAPbobsCOM.AdminInfo adminInfo = sboCompany.GetCompanyService().GetAdminInfo();
                    digits = adminInfo.PercentageAccuracy;
                    info.NumberFormat.NumberDecimalSeparator = adminInfo.DecimalSeparator;
                    info.NumberFormat.NumberGroupSeparator = adminInfo.ThousandsSeparator;
                    provider.NumberFormat.NumberDecimalSeparator = ".";
                    provider.NumberFormat.NumberGroupSeparator = ",";

                }
                catch { }
                try
                {
                    str = Convert.ToString(Math.Round(Convert.ToDouble(oDBDataSource, provider), digits), provider);
                }
                catch { }
            }
            catch { }

            return str;
        }
    }
}


public struct Paymenttype
{
    public string sType { get; set; }
    public double dbAmountUSD { get; set; }
    public double dbAmountCPL { get; set; }
    public string sTypeService { get; set; }
    public string sItemDescr { get; set; }
    public string sCurrency { get; set; }
    public double dbRate { get; set; }
    public string sCard { get; set; }
    public string sCardUSD { get; set; }
}




